package com.adobe.aem.guides.core.models;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public interface HeaderModel {

    @Inject
    List<Link> getLinks();

    default void getName() {
        System.out.println("Hello");
    }

    @Getter
    @Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
    class Link {

        @ValueMapValue
        private String text;

        @ValueMapValue
        private String link;

        @PostConstruct
        protected void init() {
            text = text.toUpperCase();
            link = link + ".html";
        }
    }
}
