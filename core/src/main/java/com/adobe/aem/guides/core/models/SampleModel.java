package com.adobe.aem.guides.core.models;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SampleModel {

    private static final Logger LOGGER = LoggerFactory.getLogger(SampleModel.class);

    @ValueMapValue
    @Default(values = StringUtils.EMPTY)    // Defines a default value to avoid NullPointerException
    private String text;

    @PostConstruct
    protected void init() {
        LOGGER.info("Sample Model Working");
        text = text.toUpperCase();
    }

    public String getText() {
        return text;
    }
}
